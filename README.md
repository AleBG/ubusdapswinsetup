# ubusdapsWinSetup

Setup for ubusdaps on Windows 10 plus WSL2, including the use of the gui revision of the surveys

## Requirements

- Windows 10 with WSL2

## Setup for Windows 10

- Check the official way of installing WSL2 in your Windows 10 machine. See the [Microsoft docs](https://docs.microsoft.com/en-us/windows/wsl/install)

- Get Ubuntu 18 (focal) from the Windows Store; optionally, also get the Windows Terminal
    + Open the Ubuntu 18 app the first time, create a user with a password, and update + upgrade the system

- Setup an X server in Windows to use gui apps from your Ubuntu install. With [this guide](https://techcommunity.microsoft.com/t5/windows-dev-appconsult/running-wsl-gui-apps-on-windows-10/ba-p/1493242) everything works:
    + [Download VcXsrv](https://sourceforge.net/projects/vcxsrv/) and install it
    + **PROVIDE ALL (PUBLIC AND PRIVATE) FIREWALL PERMISSIONS!** otherwise it won't work, and we'll have to erase the firewall inbound rules to reset
    + Run it with the default config **BUT!** check "disable access control". The config can be saved to be executed later
    + Add this to your `~/.bashrc` to allow communication between your Ubuntu 18 WSL2 and VcXsrv
    ```bash
    export DISPLAY="`grep nameserver /etc/resolv.conf | sed 's/nameserver //'`:0"
    ```
    + Add this to your Ubuntu 18 user's home: `echo xfce4-session > ~/.xsession` (don't know why, but some things don't work without this)
    + Reset your Ubuntu 18 session and check that it worked by launching a simple gui app (e.g., `xcalc`)

- Install the docker CLI in your Ubuntu 18 WSL2 installation. With [this guide](https://dev.to/felipecrs/simply-run-docker-on-wsl2-3o8) we'll install only the CLI (no need for docker Desktop):
    + [Install docker in Ubuntu 18 from the official docker repos](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)
    + [Make docker available without sudo](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)
    + Add this to your `~/.bashrc`, which starts docker when you log in (this is necessary because the Ubuntu 18 WSL2 doesn't have systemd to manage docker)
    ```bash
    if service docker status 2>&1 | grep -q "is not running"; then
        wsl.exe -d "${WSL_DISTRO_NAME}" -u root -e /usr/sbin/service docker start >/dev/null 2>&1
    fi
    ```
    + Restart your Ubuntu 18 session and check that it worked with `docker run hello-world`

- Pull the ubusdaps image: `docker pull luisalebg/ubusdaps:latest`

- Grab the ubusdaps bash wrapper for the container, and add the folder where you put it to your PATH for easy access:
```bash
mkdir ~/.bin
wget -O ~/.bin/ubusdaps "https://gitlab.com/AleBG/dotbin/-/raw/master/ubusdaps"
chmod +x ~/.bin/ubusdaps
```

- Done! Now work with ubusdaps as usual


